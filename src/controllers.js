// Bill Struct 
// {
// 	"restaurant_name": "Karahi Kitchen",
// 	"gst_no": "1243DSE42DFFF",
// 	"restaurant_address": "No. 258, WCR, 4th Satge, 4th Main, 1st Cross, Industrial Town Bangalore - 560044",
// 	"table_no": 5,
// 	"bill_no": "12123345",
// 	"bill_data": [{
// 		"item_name": "Gobi Manchurian",
// 		"item_price": 120,
// 		"item_qty": 1,
// 		"item_total": 120
// 	},
// 	{
// 		"item_name": "Mushroom Manchurian",
// 		"item_price": 120,
// 		"item_qty": 1,
// 		"item_total": 120
// 	},
// 	{
// 		"item_name": "Gulab Jamun",
// 		"item_price": 60,
// 		"item_qty": 2,
// 		"item_total": 120
// 	}],
// 	"sub_total": 360,
// 	"cgst": 3.63,
// 	"sgst": 3.63,
// 	"grand_total": 367
// }


//KOT Struct

// {
// 	"table_no": 6,
// 	"kot_no": 34,
// 	"kot_data": [{
// 		"item_name": "Gobi Manchurian",
// 		"item_qty": 1
// 	},
// 	{
// 		"item_name": "Mushroom Manchurian",
// 		"item_qty": 1
// 	},
// 	{
// 		"item_name": "Gulab Jamun",
// 		"item_qty": 2
// 	}]
// }

const escpos = require('escpos')
const fs = require('fs')

module.exports = {
	printBill(req, res) {
		try {
			const printerData = JSON.parse(fs.readFileSync('./db.json'))
			const device = printerData.p_id !== undefined ? new escpos.USB(parseInt(printerData.p_id, 16), parseInt(printerData.v_id, 16)) : new escpos.USB()

			const printer = new escpos.Printer(device);
			const billData = req.body
			device.open(() => {
				printer
					.control("LF")
					.control("LF")
					.align('ct')
					.size(1, 1)
					.style('B')
					.text(billData.restaurant_name)
					.text(`GSTIN: ${billData.gst_no}`)
					.control("LF")
					.style('NORMAL')
					.text(billData.restaurant_address)
					.control("LF")
					.tableCustom([{
							text: `Date: ${(new Date()).toString().replace(/\S+\s(\S+)\s(\d+)\s(\d+)\s.*/,'$2-$1-$3')}`,
							align: "LEFT",
							width: 0.50
						},
						{
							text: `Time: ${new Date().toLocaleTimeString()}`,
							align: "RIGHT",
							width: 0.50
						}
					])
					.tableCustom([{
							text: `Table No.: ${billData.table_no}`,
							align: "LEFT",
							width: 0.50
						},
						{
							text: `Bill No.: ${billData.bill_no}`,
							align: "RIGHT",
							width: 0.50
						}
					])
					.drawLine()
					.tableCustom([{
							text: "Item",
							align: "LEFT",
							width: 0.50
						},
						{
							text: "Rate",
							align: "RIGHT",
							width: 0.1666
						},
						{
							text: "Qty.",
							align: "RIGHT",
							width: 0.1666
						},
						{
							text: "Amount",
							align: "RIGHT",
							width: 0.1666
						}
					])
					.drawLine()

				billData.bill_data.forEach(value => {
					printer.tableCustom([{
							text: value.itemName,
							align: "LEFT",
							width: 0.50
						},
						{
							text: `${value.itemPrice}`,
							align: "RIGHT",
							width: 0.1666
						},
						{
							text: `${value.qty}`,
							align: "RIGHT",
							width: 0.1666
						},
						{
							text: `${value.netItemPrice}`,
							align: "RIGHT",
							width: 0.1666
						}
					])
				})


				printer.drawLine()
					.align("rt")
					.text(`Sub Total:\t${billData.sub_total}`)
					.drawLine()
					.align("rt")
					.text(`CGST: 2.5%\t${billData.cgst}`)
					.text(`CGST: 2.5%\t${billData.sgst}`)
					.text(`Total Bill:  \t${billData.grand_total}`)
					.drawLine()
					.size(2, 1)
					.align('ct')
					.text(`Grand Total: ${billData.grand_total}/-`)
					.control("LF")
					.control("LF")
					.size(1, 1)
					.align("ct")
					.style('b')
					.text("Where's the Food - https://wtf.menu")
					.control("LF")
					.control("LF")
					.cut()
					.close()
			})
			res.send({
				status: true,
				message: "Bill Print Successful!"
			})
		} catch (e) {
			res.send({
				status: false,
				message: `Bill Print Failed, Check printer connection. - ${e}`
			})
		}
	},
	printKOT(req, res) {
		try {
			const printerData = JSON.parse(fs.readFileSync('./db.json'))
			const device = printerData.p_id !== undefined ? new escpos.USB(parseInt(printerData.p_id, 16), parseInt(printerData.v_id, 16)) : new escpos.USB()

			const printer = new escpos.Printer(device);
			const kotData = req.body
			device.open(() => {
				printer
					.control("LF")
					.control("LF")
					.align('ct')
					.size(2, 1)
					.style('B')
					.text(`KOT No.: ${kotData.kot_no}`)
					.size(1, 1)
					.control("LF")
					.tableCustom([{
							text: `Table No.: ${kotData.table_no}`,
							align: "LEFT",
							width: 0.50
						},
						{
							text: `Time: ${new Date().toLocaleTimeString()}`,
							align: "RIGHT",
							width: 0.50
						}
					])
					.drawLine()
					.tableCustom([{
							text: "Item",
							align: "LEFT",
							width: 0.50
						},
						{
							text: "Qty.",
							align: "RIGHT",
							width: 0.50
						}
					])
					.drawLine()

				kotData.kot_data.forEach(value => {
					printer.tableCustom([{
							text: `${value.item_name}`,
							align: "LEFT",
							width: 0.50
						},
						{
							text: `${value.qty}`,
							align: "RIGHT",
							width: 0.5
						}
					])
				})

				printer.drawLine()
					.control("LF")
					.control("LF")
					.cut()
					.close()
			})
			res.send({
				status: true,
				message: "KOT Print Successful!"
			})
		} catch (e) {
			console.log(e);
			res.send({
				status: false,
				message: `KOT Print Failed, Check printer connection. - ${e}`
			})
		}
	},
	ping(req, res) {
		try {
			const printerData = JSON.parse(fs.readFileSync('./db.json'))
			const device = printerData.p_id !== undefined ? new escpos.USB(parseInt(printerData.p_id, 16), parseInt(printerData.v_id, 16)) : new escpos.USB()

			const printer = new escpos.Printer(device);
			res.send({
				status: !(printer === undefined),
				type: "Printer Connected, and ready to Print"
			})

		} catch (e) {
			res.send({
				status: false,
				type: 'Printer not found, try reconnecting or use /set_printer'
			})
		}
	},
	home(req, res) {
		res.send({
			status: 'OK'
		})
	},
	setPrinter(req, res) {
		const printer_data = {
			p_id: req.body.p_id,
			v_id: req.body.v_id
		}

		fs.writeFile('./db.json', JSON.stringify(printer_data), err => {
			if (err) {
				console.log('Error writing file', err)
				res.send({
					status: false,
					message: "Printer data not saved"
				})
			} else {
				console.log('Successfully wrote file')
				res.send({
					status: true,
					message: "Printer set sucessfully"
				})
			}
		})
	}
}