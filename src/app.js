// let helloWorld = `Hello World!`;
// console.log(`${helloWorld} this is some ES6 JavaScript code`);
// (async () => {

//     const device = await USB.getDevice();
//     const printer = await Printer.create(device);

//     await printer.text('hello');
//     await printer.cut();
//     await printer.close();

//     console.log('print job done');

// })();

const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const path = require('path')
const cors = require('cors')

const controller = require('./controllers')
const PORT = 26502

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

app.set('views', path.join(__dirname, 'views'));
app.set('view engine','pug');

app.get('/', controller.home)
app.get('/ping', controller.ping)
app.post('/set_printer', controller.setPrinter)
app.post('/bill', controller.printBill)
app.post('/kot', controller.printKOT)
app.get('/printer', (req, res) => {
    res.render("set_printer")
})

app.listen(PORT, () => console.log(`App running listening on port ${PORT}!`))